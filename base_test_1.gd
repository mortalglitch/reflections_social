extends Spatial
#var public = {}
const post_container = preload("res://ctrl_post.tscn")
const protopost = preload("res://protopost.tscn")
var post_data = []
var current_post = {}


func _ready():
	$HTTPRequest.connect("request_completed", self, "_on_request_completed")
	$HTTP_Image_Request.connect("request_completed", self, "_on_image_request_completed")


# Fetch
func _on_Button_pressed():
	$HTTPRequest.request("https://mastodon.sdf.org/api/v1/timelines/public?limit=2")
	# Requires Auth
	#$HTTPRequest.request("https://mastodon.sdf.org/api/v1/timelines/home?limit=2")


# For Standard HttpRequest completed
# Download and parse the received JSON data and feed relevant parts back into
# the post_data array
func _on_request_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	if typeof(json.result) == TYPE_ARRAY:
		print(json.result.size())

	var public = {}
	var i = 0
	for item in json.result:
		public = json.result[i]
		var account
		var account_image_url
		var content
		for key in public:
			if key == "account":
				print(typeof(public[key]))
				print(public[key]["acct"])
				
				account = public[key]["acct"]
				account_image_url = public[key]["avatar"]
			if key == "content":
				print(key)
				print(public[key])
				
				content = public[key]
		post_data.append({"acct": account, "account_image_url": account_image_url, "content": content})
		public = {}
		i += 1
	print(post_data)


# Temp Build button link
# begins submitting HTTP_Image_Request to get post profile images.
func _on_but_Build_pressed():
	for item in post_data:
		var image_url = item["account_image_url"]
		current_post = item
		$HTTP_Image_Request.request(image_url)
		print("Image HTTP called")


# Error checking isn't in place on this function could cause image errors if no profile or bad load
# https://godotengine.org/qa/40181/unpackaged-images-question?show=40229#a40229
# Circular Image: https://godotengine.org/qa/46140/how-to-create-rounded-circular-image
# Gif images seem to be broken within Godot at this time. May be able to scrape a poolbytearray for results
# Right now other items need to be worked out.
# Building process starts when image capture returns from httprequest
func _on_image_request_completed(result, response_code, headers, body):	
	var image = Image.new()
	if current_post["account_image_url"].ends_with(".jpg") or current_post["account_image_url"].ends_with(".jpeg"):
		var image_error = image.load_jpg_from_buffer(body)
		if image_error != OK:
			print("An error occurred while trying to fetch the profile image.")
	elif current_post["account_image_url"].ends_with(".png"):
		var image_error = image.load_png_from_buffer(body)
		if image_error != OK:
			print("An error occurred while trying to fetch the profile image.")
	image.resize(100,100,Image.INTERPOLATE_BILINEAR)
	var texture = ImageTexture.new()
	texture.create_from_image(image)
	var account_name = current_post["acct"]
	var post_content = current_post["content"]
	build_post(texture, account_name, post_content)
	print("image attached post built")


# Instantiate a new post and attach it to the post_container
# Show_Post function may not be needed but can be adapted.
func build_post(texture, account_name, post_content):
	var new_post_container = post_container.instance()
	$Control/ScrollContainer/post_container.add_child(new_post_container)
	new_post_container.set_account_image(texture)
	new_post_container.set_post_account(account_name)
	new_post_container.set_post_content(post_content)
	new_post_container.show_post()


func _protopost():
	var protopost1 = protopost.instance()
	$Control/ScrollContainer/post_container.add_child(protopost1)


func _on_but_Build2_pressed():
	_protopost()
