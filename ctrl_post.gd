extends Control


func _ready():
	pass # Replace with function body.


# Setter for profile image
func set_account_image(texture):
	$vbc_post/account_avatar_image.texture = texture


# Setter for account name
func set_post_account(account_name):
	$vbc_post/lbl_account_name.set_text(account_name)


# Setter for content block
func set_post_content(content):
	$vbc_post/lbl_content.set_text(content)


# Sets the post size to prevent bunching.
# May need work later
func show_post():
	#required for frame calc prior to trying to grab size
	yield(get_tree(), "idle_frame")
	print(rect_size)
	var new_size = get_node("vbc_post").get_size()
	rect_min_size = new_size
	print (new_size)

